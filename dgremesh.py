#!/usr/bin/python
"""

DGreMesh - Distributed GRE Mesher

Listens for other mesh nodes on the same subnet and creates GRE tunnels to them
- Remember to enable STP on bridge!

usage:
dgremesh [adapter ip/subnet] [udp port] [bridge device] [known node 1] [known node 2] ... [known node n]
dgremesh --help

e.g. dgremesh 10.0.65.3/18 5111 br0 10.0.65.1 10.0.65.2 10.0.65.3

"""

import os, sys, socket, getopt, time
from threading import Thread
import dgm_ip, dgm_udp, dgm_util

def main():
    """
    Main
    """
    # parse command line options
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["help"])
        
        # process options
        for o, a in opts:
            if o in ("-h", "--help"):
                raise Exception("You need help?")
        
        # process arguments
        params = {}
        params['nodes'] = []
        if len(args) < 4:
            raise Exception("Not enough arguments")
        for i in range(len(args)):
            if i == 0:
                 params['ipsubnet'] = args[0]
                 params['port'] = int(args[1])
                 params['bridge'] = args[2]
            elif i > 2:
                # 3 onwards
                params['nodes'].append( args[i] )
        
        # parse
        t = params['ipsubnet'].split('/')
        params['ip'] = t[0]
        params['ipnum'] = dgm_ip.dottedQuadToNum(params['ip'])
        params['subnetbits'] = int(t[1])
        if params['subnetbits'] <= 0 or params['subnetbits'] > 30:
            raise Exception("Invalid subnet bits")
        params['networknum'] = dgm_ip.networkMask(params['ip'], params['subnetbits'])

        # verify that nodes are in same subnet
        for node in params['nodes']:
            if not dgm_ip.addressInNetwork(dgm_ip.dottedQuadToNum(node), params['networknum'], params['subnetbits']):
                raise Exception("Node " + node + " not part of subnet!")  

        # GLOBAL DEBUG FLAG
        #params['debug'] = True
        params['debug'] = False
        
    except Exception, msg:
        print msg
        print __doc__
        sys.exit(0)

    # params are ready, start threads
    # print params
    
    beacon_sock = dgm_udp.beacon_socket(params)
    print beacon_sock
    
    def recv_thread():
        """
        Receives and handles packets
        """
        beacon_sock.listen()
        
    rthread = Thread(target = recv_thread)
    rthread.daemon = True
    rthread.start()
    
    tunlist = set()
    
    # loop every 5s
    while True:
        # send out beacons
        if params['debug']:
            print "Sending beacons"
        beacon_sock.send_beacons()
        
        # obtain list of known alive nodes, and create gre tunnels for new nodes
        iplist = beacon_sock.alive_nodes.getlist()

        if params['debug']:
            print iplist
        
        for ipnum in iplist:
            if ipnum not in tunlist:
                tunlist.add(ipnum)
                ip_string = dgm_ip.numToDottedQuad(ipnum)
                if params['debug']:
                    print "Add GRE tunnel to " + ip_string
                # actually add GRE
                dgm_util.config_gre(params['bridge'], params['ip'], ip_string)
    
        time.sleep(5)



    











#
# Call main
#
if __name__ == "__main__":
    main()

