import sys, os, socket, dgm_ip, threading
from dgm_util import *
import cPickle as pickle


class node_tracker(object):
    """
    Node lifetime tracking
    - Synchronized
    - Essentially maps of IP : timestamp
    """

    def __init__(self, timeout):
        self.__timeout = timeout
        self.__nodes = {}
        self.__lock = threading.Lock()
        
    def cleanup(self):
        """
        Removes timed out nodes
        - expected to be within locks
        """
        current = now()
        # make a copy of dict so that we don't modify original while iterating
        copynode = self.__nodes.copy()
        for node, timestamp in copynode.iteritems():
            if current - timestamp > self.__timeout:
                # delete node
                del self.__nodes[node]
        
    def update(self, numips):
        """
        Updates ip in map
        - removes timed out nodes
        - refreshes ip timestamp
        
        numip -- list of integer IPs
        """
        try:
            self.__lock.acquire()
            
            for ip in numips:
                if not isinstance(ip, int):
                    raise Exception("Unexpected non-int type")
                self.__nodes[ip] = now()
                
            self.cleanup()

        finally:
            self.__lock.release()
        
    def getlist(self):
        """
        Return list of nodes
        - removes timed out nodes
        return -- list of ip integers
        """
        try:
            self.__lock.acquire()
            # cleanup before returning list
            self.cleanup()
            
            ret = []
            for node, timestamp in self.__nodes.iteritems():
                ret.append(node)
            return ret
            
        finally:
            self.__lock.release()
        

class beacon_socket(object):
    """
    Internal socket object to send/recv UDP from a fixed ip/port, decode packets, etc
    """

    def __init__(self, params):
        """
        Constructor
        
        params -- parameter map
        """
        # constants
        self.max_nodes_in_packet = 100  # unlikely we exceeed this limit...
        self.node_timeout = 60
        
        self.params = params
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        
        # take out stuff we frequently use
        self.ip = self.params['ip']
        self.ipnum = self.params['ipnum']
        self.port = self.params['port']
        self.networknum = self.params['networknum']
        self.subnetbits = self.params['subnetbits']
        
        # bind
        self.sock.bind((self.ip, self.port))

        # node tracking
        self.alive_nodes = node_tracker(self.node_timeout)
        self.known_nodes = node_tracker(self.node_timeout)      
        

    def listen(self):
        """
        Waits for UDP packets, calls callback for each packet. Blocks and loops forever.
        """
        while True:
            try:
                data, addr = self.sock.recvfrom(1024)
                (recvip, recvport) = addr
                # verify ip and port
                if not recvport == self.port:
                    continue
                if not dgm_ip.addressInNetwork(dgm_ip.dottedQuadToNum(recvip), self.networknum, self.subnetbits):
                    continue
                if len(data) > 1023 or len(data) == 0:
                    continue
                    
                self.process_beacon(recvip, data)
                    
            except Exception, msg:
                print "Exception: " + msg
                printstacktrace()
            
            
    def process_beacon(self, ip, packdata):
        """
        Process incoming packet data
        
        ip -- string ip
        packdata -- raw packet data
        
        Data format:
        [int, int, int, int ... ]
        Expected length: 
        """
        # assume pickle data is trusted, don't run over untrusted / public subnets
        iplist = pickle.loads(packdata)    # may throw PicklingError
        # verify obj is a list of ints
        if not isinstance(iplist, list):
            raise Exception("unpickled obj not a list")
        
        candidate_ipnums = []
        for numip in iplist:
            if not isinstance(numip, int):
                raise Exception("non int found in list")
            # check if ip is in subnet
            if not dgm_ip.addressInNetwork(numip, self.networknum, self.subnetbits):
                print "received ip " + dgm_ip.numToDottedQuad(numip) + " not in subnet!"
            # ensure its not self
            if not numip == self.ipnum:
                candidate_ipnums.append(numip)

        # everything passed, update node tracker
        ipnum = dgm_ip.dottedQuadToNum(ip)
        candidate_ipnums.append(ipnum)
        self.known_nodes.update(candidate_ipnums)

        if self.params['debug']:
            print "Received beacon from " + ip
            print "Known nodes: " + str(self.known_nodes.getlist())
                
        
        # we heard beacon from an alive node, take note
        self.alive_nodes.update([ipnum])
        return
        
    
    def send_beacons(self):
        """
        Create and sends beacon packets of 'alive nodes' to all 'known nodes'.
        Typically this is called every interval.
        
        ip -- string ip to send to
        """
        # seed nodes never expire, re-update them
        seeds = []
        for ip in self.params['nodes']:
            # ensure its not self
            if not dgm_ip.dottedQuadToNum(ip) == self.ipnum:
                seeds.append( dgm_ip.dottedQuadToNum(ip) )
        self.known_nodes.update(seeds)
        
        alivelist = self.alive_nodes.getlist()

        if self.params['debug']:
            print "Alivelist: " + str(alivelist)


        if len(alivelist) > self.max_nodes_in_packet:
            print "WARN: alivelist > max_nodes_in_packet of " + str(self.max_nodes_in_packet)
        del alivelist[self.max_nodes_in_packet:]
        data = pickle.dumps(alivelist)        
        knownlist = self.known_nodes.getlist()
        
        for ipnum in knownlist:
            ip = dgm_ip.numToDottedQuad(ipnum)
            if self.params['debug']:
                print "Sending beacon to " + ip
            self.sock.sendto(data, (ip, self.port))
        
        return
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
