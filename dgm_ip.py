import os, sys, socket, struct



def test():
    
    address = dottedQuadToNum("10.0.63.1")
    networka = networkMask("10.0.64.0",18)
    networkb = networkMask("10.0.0.0",18)
    print (bin(networka), bin(networkb))
    print (address,networka,networkb)
    print addressInNetwork(address,networka,18)
    print addressInNetwork(address,networkb,18)

#
# Netmask helpers
#

def makeMask(n):
    """
    Return a mask of n bits as a integer
    
    n -- bits
    returns -- unsigned long mask
    """
    return ((2L <<n-1) - 1) << (32-n)

def dottedQuadToNum(ip):
    """
    Convert decimal dotted quad string to unsigned integer
    Order of bytes similar to that of ip
    
    ip -- string ip address
    returns -- unsigned int of ip address
    """
    # note: big endian
    t1 = struct.unpack('>I',socket.inet_aton(ip))[0]
    return t1
    
def numToDottedQuad(ipnum):
    """
    Convert unsigned int to IP string
    
    ipnum -- numeric ip
    returns -- string IP
    """
    # note: big endian
    return str(socket.inet_ntoa(struct.pack('>I',ipnum)))
    

def networkMask(ip,bits):
    """
    Obtains network portion of ip address
    
    ip -- ip address string
    bits -- number of bits for net mask
    returns -- unsigned int of network address
    """ 
    return dottedQuadToNum(ip) & makeMask(bits)

def addressInNetwork(ip,net,bits):
   """
   Is an address in a network?
   
   ip -- unsigned int ip address
   net -- unsigned int network address
   bits -- num bits of network address
   returns -- boolean   
   """
   return ip & makeMask(bits) == net
   
   
   
   
   
   
